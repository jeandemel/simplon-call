const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const fs = require('fs');

const roomFile = './rooms';
const banFile = './.bannedIp';
let roomList = fs.readFileSync(roomFile, 'utf8').split('\n').map(name => ({ id: name.trim() + '-' + Date.now(), name:name.trim() }));
let bannedIp = fs.readFileSync(banFile, 'utf8').split('\n').map(line => line.trim());
const adminPass = fs.readFileSync('./.admin', 'utf8').trim();

const port = process.env.PORT || 3000;


let users = [];
io.on('connection', socket => {
    
    let admin = false;
    if(bannedIp.includes(socket.request.connection.remoteAddress)){
        socket.emit('authError');        
        return;
    }
    if (socket.handshake.query.password) {
        if (socket.handshake.query.password === adminPass) {
            admin = true;
        } else {
            socket.emit('authError');
            return;
        }
    }
    const user = {
        id: socket.id,
        room: null,
        name: socket.handshake.query.name,
        invite: null,
        admin,
        help: false,
        userId: socket.handshake.query.userId ? socket.handshake.query.userId:Date.now()+''+Math.floor(Math.random()*1000)
    };
    
    const connected = users.find(item => item.userId == user.userId);
    if(connected && io.clients().sockets[connected.id]) {
        io.clients().sockets[connected.id].disconnect(true);
        
        //users = users.filter(item => item.userId == user.userId);
    } 
    
    users.push(user);

    io.emit('users', users);
    socket.emit('user', user);
    socket.emit('roomList', roomList);

    socket.on('changeRoom', room => {
        user.room = room;
        io.emit('users', users);
        socket.emit('user', user);
    });

    socket.on('changeName', name => {
        user.name = name;
        io.emit('users', users);
        socket.emit('user', user);
    });

    socket.on('toggleHelp', () => {
        user.help = !user.help;
        io.emit('users', users);
        socket.emit('user', user);
    });

    socket.on('globalMessage', message => {
        if(user.admin) {
            socket.broadcast.emit('alert', message);
        }
    });

    socket.on('moveAllTo', room => {
        if(user.admin) {
            for(let user of users) {
                if(!user.admin) {
                    user.room = room.id;
                    socket.broadcast.to(user.id).emit('user', user);
                }
            }
            io.emit('users', users);
        }
    })

    socket.on('inviteUser', userToInvite => {
        userToInvite = users.find(item => item.id === userToInvite.id);
        if (userToInvite) {
            userToInvite.invite = user.id;
            user.invite = 'waiting';
            socket.broadcast.to(userToInvite.id).emit('user', userToInvite);
            io.emit('users', users);
            socket.emit('user', user);
        }
    });


    socket.on('acceptInvite', () => {
        const userToCall = users.find(item => item.id === user.invite);
        if (userToCall) {
            userToCall.invite = null;
            user.room = user.id + '-' + userToCall.id;
            userToCall.room = user.id + '-' + userToCall.id;
            socket.broadcast.to(userToCall.id).emit('user', userToCall);

        }
        user.invite = null;
        socket.emit('user', user);
        io.emit('users', users);
    });


    socket.on('refuseInvite', () => {
        const userToCall = users.find(item => item.id === user.invite);
        if (userToCall) {
            userToCall.invite = null
            socket.broadcast.to(userToCall.id).emit('user', userToCall);
        }
        user.invite = null;
        socket.emit('user', user);
        io.emit('users', users);
    });

    socket.on('disconnect', () => {
        users = users.filter(item => user !== item);
        io.emit('users', users);
    });


    socket.on('moveUser', userToMove => {
        if (admin) {
            const user = users.find(item => item.id === userToMove.id);
            if (user) {
                user.room = userToMove.room;
                io.to(user.id).emit('user', user);
                io.emit('users', users);
            }
        }
    });

    socket.on('kickUser', userToKick => {
        if (admin) {
            io.clients().sockets[userToKick.id].disconnect(true);
        }
    });

    socket.on('banUser', userToBan => {
        if (admin) {
            const client = io.clients().sockets[userToBan.id];
            bannedIp.push(client.request.connection.remoteAddress);
            fs.writeFileSync(banFile, bannedIp.join('\n'));
            client.disconnect(true);
        }
    });

    socket.on('renameRoom', room => {
        if (admin) {
            roomList.find(item => item.id === room.id).name = room.name;
            fs.writeFileSync(roomFile, roomList.map(item => item.name).join('\n'));
            io.emit('roomList', roomList);
        }
    });

    socket.on('removeRoom', room => {
        if (admin) {
            roomList = roomList.filter(item => item.id !== room.id);
            fs.writeFileSync(roomFile, roomList.map(item => item.name).join('\n'));
            io.emit('roomList', roomList);
        }
    });
    
    socket.on('addRoom', roomName => {
        if (admin) {
            roomList.push({name: roomName, id: roomName + '-' + Date.now()});
            fs.writeFileSync(roomFile, roomList.map(item => item.name).join('\n'));
            io.emit('roomList', roomList);
        }
    });

});



app.use(express.static('dist'));

server.listen(port, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:' + port);
});