import React, { useEffect } from 'react';
import './App.css';

import { Provider } from 'react-redux'
import { store } from './store'
import { closeSocket } from './service/socket-service'
import Main from './components/Main';
import Container from "@material-ui/core/Container";

import { unstable_createMuiStrictModeTheme as createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { amber, red } from '@material-ui/core/colors';


const theme = createMuiTheme({
  palette: {
    primary: red,
    secondary: amber
  },
});


function App() {
  useEffect(() => {
    return () => {
      closeSocket();
    }
  }, [])

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Container>
          <Main />
        </Container>
      </ThemeProvider>
    </Provider>


  )
}

export default App;
