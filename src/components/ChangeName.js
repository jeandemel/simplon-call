import React, { useState } from 'react'
import { TextField, Button, Grid, FormControlLabel, Checkbox } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux';
import { changeName } from '../service/socket-service';
import { changeUserName } from '../store/socket-slice';

export default function ChangeName() {
    const user = useSelector(state => state.socket.user);
    const [form, setForm] = useState({
        name:user.name,
        password: '',
        admin: false
    });

    const dispatch = useDispatch();

    const handleChange = event => {
        if(event.target.name === 'admin') {
            setForm({...form, admin:event.target.checked});

        }else {
            setForm({...form, [event.target.name]:event.target.value});

        }
    }
    const handleSubmit = event => {
        event.preventDefault();
        localStorage.setItem('username', form.name);
        if (user.name) {
            changeName(form.name);
        } else {
            dispatch(changeUserName(form.name));
        }
        if(form.admin) {
            localStorage.setItem('adminPass', form.password);
        }

    }
    return (
        <form onSubmit={handleSubmit}>
            <Grid container className="root">
                <Grid item xs={5}>
                    <TextField fullWidth label="Votre nom" variant="outlined" name="name" value={form.name} onChange={handleChange} />
                    
                    {form.admin && <TextField fullWidth label="Mot de Passe" variant="outlined" name="password" type="password" value={form.password} onChange={handleChange} /> }
                    
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={form.admin}
                                onChange={handleChange}
                                name="admin"
                                color="primary"
                            />
                        }
                        label="Admin"
                    />
                </Grid>
                <Grid item xs={2}>
                    <Button type="submit" variant="contained" color="secondary" size="large">Go</Button>
                </Grid>
            </Grid>
        </form>
    )
}
