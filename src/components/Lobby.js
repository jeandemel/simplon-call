import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {  connectSocket } from '../service/socket-service';
import RoomList from './RoomList';
import { Chip, Typography } from '@material-ui/core';
import StarRateRoundedIcon from '@material-ui/icons/StarRateRounded';
import MenuUser from './MenuUser';


export default function Lobby() {
    const curUser = useSelector(state => state.socket.user);
    const users = useSelector(state => state.socket.users);
    const [anchorEl, setAnchorEl] = useState(null);
    const [selected, setSelected] = useState();

    useEffect(() => {
        connectSocket();
    }, []);

    const userColor = user => {
        if(user.help) {
            return 'secondary';
        }
        if(user.room)  {
            return 'primary';
        }
        return 'default';
    }

    const handleClick = (event, user) => {
        setAnchorEl(event.currentTarget);
        setSelected(user);
    };

    const handleClose = () => {
        setAnchorEl(null);
        setSelected(null);
    };

    return (
        <div>
            <div id="call"></div>
            <Typography component="h2" variant="h6">Utilisateur·ice·s</Typography>
            {users.slice().sort((a,b) => a.name.localeCompare(b.name)).map(user =>
                <Chip icon={user.admin? <StarRateRoundedIcon />:null} onClick={event => handleClick(event,user)} key={user.id} label={user.name} clickable disabled={user.id === curUser.id} color={userColor(user)} />

            )}
            <MenuUser selected={selected} anchorEl={anchorEl} onClose={handleClose} />
            <RoomList />
        </div>
    )
}
