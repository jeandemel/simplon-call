import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import ChangeName from './ChangeName';
import Lobby from './Lobby';
import { AppBar, Toolbar, Typography, Button, Box, makeStyles, Modal, Card, CardContent } from '@material-ui/core';
import InviteDialog from './dialogs/InviteDialog';
import RemoveConfirm from './dialogs/RemoveConfirm';
import BanConfirm from './dialogs/BanConfirm';
import RenameDialog from './dialogs/RenameDialog';
import AddDialog from './dialogs/AddDialog';
import { toggleHelp } from '../service/socket-service';
import AlertMessage from './dialogs/AlertMessage';
import GlobalMessage from './dialogs/GlobalMessage';
import { openFeedback } from "../store/socket-slice";


const useStyles = makeStyles((theme) => ({
    title: {
        flexGrow: 1,
    },
    headerMargin: {
        marginTop: 75
    },
    modal: {
        width: 500,
        margin: '50px auto'
    }
}));

export default function Main() {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const user = useSelector(state => state.socket.user);
    const dispatch = useDispatch();

    const openGlobal = () => {
        dispatch(openFeedback({feedback: 'globalMsg', value:true}))
    }

    return (
        <div>
            {user.name ?
                <>
                    <AppBar color="primary">
                        <Toolbar>
                            <Typography variant="h6" className={classes.title}>
                                Simplon Call
                        </Typography>
                        <Button variant="contained" color={user.help ?'secondary':'primary'} onClick={() => toggleHelp()}>
                            Help !
                        </Button>
                        {user.admin &&
                        <Button color="inherit" onClick={openGlobal}>Message Global</Button>
                        }
                        <Button color="inherit" onClick={() => setOpen(true)}>Changer Nom</Button>
                        </Toolbar>
                    </AppBar>
                    <Box className={classes.headerMargin}>
                        <Lobby />
                    </Box>
                    <Modal open={open} onClose={() => setOpen(false)}>
                        <Card className={classes.modal}>
                            <CardContent>
                                <Typography variant="h5">
                                    Changer de Nom
                                </Typography>
                                <ChangeName />
                            </CardContent>
                        </Card>
                    </Modal>
                </> :
                <ChangeName />}
                <InviteDialog />
                <RemoveConfirm />
                <RenameDialog />
                <AddDialog />
                <BanConfirm />
                <AlertMessage />
                <GlobalMessage />
        </div>
    )
}
