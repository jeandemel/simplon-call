import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Menu, MenuItem, MenuList } from '@material-ui/core';
import { inviteUser, kickUser, moveUser } from '../service/socket-service';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import { openFeedback } from '../store/socket-slice';

export default function MenuUser({selected, anchorEl, onClose}) {
    const [subAnchor, setSubAnchor] = useState(null);
    const curUser = useSelector(state => state.socket.user);
    const rooms = useSelector(state => state.socket.roomList);
    const dispatch = useDispatch()

    const handleClose = () => {
        setSubAnchor(null);
        onClose();
    }

    const invite = () => {
        inviteUser(selected);
        handleClose();
    }

    const kick = () => {
        kickUser(selected);
        handleClose();
    }

    const ban = () => {
        dispatch(openFeedback({feedback: 'ban', value: selected}));
        handleClose();
    }

    return (
        <>
        <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
        >
            <MenuItem disabled={selected?.invite} onClick={invite}>Inviter en Visio privée</MenuItem>
            {curUser.admin && 
            <MenuList>
                <MenuItem onClick={() => kick()}>Expulser</MenuItem>
                <MenuItem onClick={() => ban()}>Bannir</MenuItem>
                <MenuItem onClick={event => setSubAnchor(event.currentTarget)}>Transférer vers... <ArrowRightIcon /></MenuItem>
            </MenuList>
            
            }
        </Menu>
        <Menu
            onClose={handleClose}
            open={Boolean(subAnchor)}
            anchorEl={subAnchor}
            anchorOrigin={{
                vertical: "top",
                horizontal: "right"
              }}
            transformOrigin={{
            vertical: "top",
            horizontal: "left"
            }}
        >
            <MenuList>
                {rooms.map(room => 
                <MenuItem key={room.id} onClick={() => moveUser(selected, room.id)}>{room.name}</MenuItem>
                )}
            </MenuList>
        </Menu>
        </>
    )
}
