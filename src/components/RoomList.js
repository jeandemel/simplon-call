import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Grid, Button, Typography, Chip, IconButton } from '@material-ui/core';

import { changeRoom } from '../service/socket-service';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RoomMenu from './RoomMenu';
import { openFeedback } from '../store/socket-slice';


const initialState = {
    mouseX: null,
    mouseY: null,
    room: null
};

export default function RoomList() {
    const rooms = useSelector(state => state.socket.roomList);
    const user = useSelector(state => state.socket.user);
    const users = useSelector(state => state.socket.users);
    const [mousePos, setMousePos] = useState(initialState);
    const dispatch = useDispatch();

    const usersInRoom = (room) => {
        return users.filter(user => user.room === room.id);
    }

    const handleContext = (event, room) => {
        if (user.admin) {
            event.preventDefault();
            setMousePos({
                mouseX: event.clientX - 2,
                mouseY: event.clientY - 4,
                room
            });
        }
    }

    const openAddDialog = () => dispatch(openFeedback({feedback: 'addRoom', value: true}));
    const menuClose = () => {
        setMousePos(initialState)
    }

    return (
        <>
            <Grid spacing={3} container>
                <Grid xs={12} item>
                    <Typography component="h2" variant="h4" >
                        Liste des Salles
                        {user.admin && 
                        <IconButton onClick={openAddDialog} aria-label="add" color="default">
                            <AddCircleIcon />
                        </IconButton>
                        }
                    </Typography>

                </Grid>
                {rooms.map(room => (
                    (!room.name.includes('*') || user.admin) &&
                    <Grid item xs={6} key={room.id}>

                        <Button onContextMenu={event => handleContext(event, room)} disabled={user.room === room.id} fullWidth variant="outlined" onClick={() => changeRoom(room.id)}>{room.name} ({usersInRoom(room).length})</Button>
                        {usersInRoom(room).map(user =>
                            <Chip key={user.id} label={user.name} variant="outlined" size="small" color={user.help ? 'secondary':'default'} />
                        )}
                    </Grid>

                ))}
            </Grid>
            <RoomMenu pos={mousePos} handleClose={menuClose} />

        </>
    )
}
