import React from 'react'
import { MenuItem, Menu } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import { openFeedback } from '../store/socket-slice';
import { moveAllTo } from '../service/socket-service';


export default function RoomMenu({pos, handleClose}) {
    const dispatch = useDispatch();

    const rmClick = () => {
        dispatch(openFeedback({feedback: 'rmRoom',value: pos.room }));
        handleClose();
    }
    

    const renameClick = () => {
        dispatch(openFeedback({feedback: 'renameRoom',value: pos.room }));
        handleClose();
    }

    const moveAllClick = () => {
        moveAllTo(pos.room);
        handleClose();
    }

    return (
        <Menu
        open={pos.mouseY !== null}
        onClose={handleClose}
        anchorReference="anchorPosition"
        anchorPosition={
          pos.mouseY !== null && pos.mouseX !== null
            ? { top: pos.mouseY, left: pos.mouseX }
            : undefined
        }
      >
          <MenuItem onClick={moveAllClick}>Transférer tou·te·s ici</MenuItem>
          <MenuItem onClick={renameClick}>Renommer la salle</MenuItem>
          <MenuItem onClick={rmClick}>Supprimer la salle</MenuItem>
      </Menu>
    )
}
