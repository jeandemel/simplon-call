import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeFeedback } from '../../store/socket-slice';
import { addRoom } from "../../service/socket-service";
import { Dialog, DialogTitle,  DialogActions, Button, DialogContent, TextField } from '@material-ui/core';


export default function AddDialog() {
    const dialogAdd = useSelector(state => state.socket.feedbacks.addRoom);
    const [name, setName] = useState('');
    const dispatch = useDispatch();

    const confirm = () => {
        addRoom(name);
        dispatch(closeFeedback('addRoom'));
    };

    const cancel = () => {
        dispatch(closeFeedback('addRoom'));
    }

    const handleChange = event => {
        setName(event.target.value);
    }

    return (
        <>
          <Dialog

            disableBackdropClick
            disableEscapeKeyDown
            open={Boolean(dialogAdd)}
            aria-labelledby="add-dialog-title"
          >
            <DialogTitle id="add-dialog-title">Renommer la salle</DialogTitle>
            <DialogContent>
                <TextField label="Nom de la Salle" variant="outlined" name="name" type="text" value={name} onChange={handleChange} />
            </DialogContent>
            <DialogActions>
              <Button onClick={cancel} color="default">
                Annuler
              </Button>
              <Button onClick={confirm} color="secondary">
                Confirmer
              </Button>
            </DialogActions>
          </Dialog>
        </>
    )
}
