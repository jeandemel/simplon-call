import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeFeedback } from '../../store/socket-slice';
import { Dialog, DialogTitle, DialogContentText, Button, DialogActions, DialogContent } from '@material-ui/core';

export default function AlertMessage() {
    const msg = useSelector(state => state.socket.feedbacks.alertMsg);
    const dispatch = useDispatch();

    const ok = () => {
        dispatch(closeFeedback('alertMsg'));
    }

    return (
        <>
          <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={Boolean(msg)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Message Global</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {msg}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={ok} color="default">
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        </>
    )
}
