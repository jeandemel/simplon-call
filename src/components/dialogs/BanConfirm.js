import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeFeedback } from '../../store/socket-slice';
import { banUser } from '../../service/socket-service';
import { Dialog, DialogTitle, DialogContentText, Button, DialogActions, DialogContent } from '@material-ui/core';

export default function BanConfirm() {
    const ban = useSelector(state => state.socket.feedbacks.ban);
    const dispatch = useDispatch();

    const confirm = () => {
        banUser(ban);
        dispatch(closeFeedback('ban'));
    };

    const cancel = () => {
        dispatch(closeFeedback('ban'));
    }

    return (
        <>
          <Dialog

            disableBackdropClick
            disableEscapeKeyDown
            open={Boolean(ban)}
            aria-labelledby="ban-dialog-title"
            aria-describedby="ban-dialog-description"
          >
            <DialogTitle id="ban-dialog-title">Bannir l'utilisateur·ice</DialogTitle>
            <DialogContent>
              <DialogContentText id="ban-dialog-description">
                Souhaitez-vous vraiment bannir {ban?.name}.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={cancel} color="default">
                Annuler
              </Button>
              <Button onClick={confirm} color="secondary">
                Confirmer
              </Button>
            </DialogActions>
          </Dialog>
        </>
    )
}
