import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeFeedback } from '../../store/socket-slice';
import { globalMessage } from "../../service/socket-service";
import { Dialog, DialogTitle,  DialogActions, Button, DialogContent, TextField } from '@material-ui/core';

export default function GlobalMessage() {
    const globMsg = useSelector(state => state.socket.feedbacks.globalMsg);
    const [msg, setMsg] = useState('');

    const dispatch = useDispatch();

    const confirm = () => {
        globalMessage(msg);
        dispatch(closeFeedback('globalMsg'));
    };

    const cancel = () => {
        dispatch(closeFeedback('globalMsg'));
    }

    const handleChange = event => {
        setMsg(event.target.value);
    }

    return (
        <>
          <Dialog

            disableBackdropClick
            disableEscapeKeyDown
            open={Boolean(globMsg)}
            aria-labelledby="global-dialog-title"
          >
            <DialogTitle id="global-dialog-title">Envoyer un message global</DialogTitle>
            <DialogContent>
                <TextField label="Message à Envoyer" multiline variant="outlined" name="msg" type="text" value={msg} onChange={handleChange} />
            </DialogContent>
            <DialogActions>
              <Button onClick={cancel} color="default">
                Annuler
              </Button>
              <Button onClick={confirm} color="secondary">
                Confirmer
              </Button>
            </DialogActions>
          </Dialog>
        </>
    )
}
