import React from 'react'
import { Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { refuseInvite, acceptInvite } from '../../service/socket-service';

export default function InviteDialog() {
    const invite = useSelector(state => state.socket.feedbacks.invite);
    
    return (
        <>
          <Dialog

            disableBackdropClick
            disableEscapeKeyDown
            open={Boolean(invite)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Invitation visio</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {invite?.name} souhaite commencer une visio privée avec vous.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={refuseInvite} color="default">
                Refuser
              </Button>
              <Button onClick={acceptInvite} color="secondary" autoFocus>
                Accepter
              </Button>
            </DialogActions>
          </Dialog>
        </>
      );
}
