import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeFeedback } from '../../store/socket-slice';
import { removeRoom } from '../../service/socket-service';
import { Dialog, DialogTitle, DialogContentText, Button, DialogActions, DialogContent } from '@material-ui/core';

export default function RemoveConfirm() {
    const rmRoom = useSelector(state => state.socket.feedbacks.rmRoom);
    const dispatch = useDispatch();

    const confirm = () => {
        removeRoom(rmRoom);
        dispatch(closeFeedback('rmRoom'));
    };

    const cancel = () => {
        dispatch(closeFeedback('rmRoom'));
    }

    return (
        <>
          <Dialog

            disableBackdropClick
            disableEscapeKeyDown
            open={Boolean(rmRoom)}
            aria-labelledby="confirm-dialog-title"
            aria-describedby="confirm-dialog-description"
          >
            <DialogTitle id="confirm-dialog-title">Supprimer la salle</DialogTitle>
            <DialogContent>
              <DialogContentText id="confirm-dialog-description">
                Souhaitez vous vraiment supprimer la salle {rmRoom?.name}.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={cancel} color="default">
                Annuler
              </Button>
              <Button onClick={confirm} color="secondary">
                Confirmer
              </Button>
            </DialogActions>
          </Dialog>
        </>
    )
}
