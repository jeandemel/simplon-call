import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeFeedback } from '../../store/socket-slice';
import { renameRoom } from "../../service/socket-service";
import { Dialog, DialogTitle,  DialogActions, Button, DialogContent, TextField } from '@material-ui/core';

export default function RenameDialog() {
    const roomToRename = useSelector(state => state.socket.feedbacks.renameRoom);
    const [name, setName] = useState('');


    useEffect(() => {
      if(roomToRename) {
        setName(roomToRename.name);
      }
    }, [roomToRename])

    const dispatch = useDispatch();

    const confirm = () => {
        renameRoom({...roomToRename, name});
        dispatch(closeFeedback('renameRoom'));
    };

    const cancel = () => {
        dispatch(closeFeedback('renameRoom'));
    }

    const handleChange = event => {
        setName(event.target.value);
    }

    return (
        <>
          <Dialog

            disableBackdropClick
            disableEscapeKeyDown
            open={Boolean(roomToRename)}
            aria-labelledby="rename-dialog-title"
          >
            <DialogTitle id="rename-dialog-title">Renommer la salle</DialogTitle>
            <DialogContent>
                <TextField label="Nom de la Salle" variant="outlined" name="name" type="text" value={name} onChange={handleChange} />
            </DialogContent>
            <DialogActions>
              <Button onClick={cancel} color="default">
                Annuler
              </Button>
              <Button onClick={confirm} color="secondary">
                Confirmer
              </Button>
            </DialogActions>
          </Dialog>
        </>
    )
}
