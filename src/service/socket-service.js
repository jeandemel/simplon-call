import { store } from "../store";
import { updateUser, updateUsersList, updateRoomList, openFeedback, closeFeedback, changeUserName } from "../store/socket-slice";
import io from "socket.io-client";
let socket;

let jitsiApi;

export function connectSocket() {
    let query = {
        name: store.getState().socket.user.name
    }
    if(localStorage.getItem('userId')) {
        query.userId = localStorage.getItem('userId');
    }
    if(localStorage.getItem('adminPass')) {
        query.password = localStorage.getItem('adminPass');
    }
    socket = io('', {
        query,
        reconnectionDelay: 1000,
        
    });

    socket.on('disconnect', () => {
        if(jitsiApi) {
            jitsiApi.dispose();

        }
        store.dispatch(changeUserName(''));
        socket.close();
    });

    socket.on('authError', () => {
        socket.close();
        if(jitsiApi) {
            jitsiApi.dispose();
        }
        localStorage.removeItem('adminPass');
        store.dispatch(changeUserName(''));
    });

    socket.on('user', user => {
        let curUser = store.getState().socket.user;
        localStorage.setItem('userId', user.userId);
        if(curUser.name !== user.name) {
            if(jitsiApi) {
                jitsiApi.executeCommand('displayName', user.name);
            }
        }
        if(user.invite && user.invite !== 'waiting') {
            const inviter = store.getState().socket.users.find(item => user.invite !== item.id);
            if(inviter){
                store.dispatch(openFeedback({feedback: 'invite', value: inviter}));
            } else {
                refuseInvite();
            }
        }
        if(user.room && user.room !== curUser.room) {
            const options = {
                roomName: user.room,
                height: 700,
                parentNode: document.querySelector('#call'),
                userInfo: {
                    displayName: user.name
                },
                configOverwrite: {
                    disableDeepLinking: true,
                }
            };
            if(jitsiApi) {
                jitsiApi.dispose();
            }
            jitsiApi = new window.JitsiMeetExternalAPI(process.env.REACT_APP_JITSI_URL, options);
            jitsiApi.addEventListener('readyToClose', () => {
                jitsiApi.dispose();
                changeRoom(null);
            });
            jitsiApi.addEventListener('videoConferenceJoined', () => {
                jitsiApi.executeCommand('displayName', user.name)
            })
        }
        store.dispatch(updateUser(user));
    });
    socket.on('users', users => store.dispatch(updateUsersList(users)));
    socket.on('roomList', rooms => store.dispatch(updateRoomList(rooms)));
    socket.on('alert', msg => store.dispatch(openFeedback({feedback:'alertMsg', value:msg})));

}
export function changeRoom(room) {
    socket.emit('changeRoom', room);
}

export function moveUser(user, room) {
    socket.emit('moveUser', {...user, room});
}
export function kickUser(user) {
    socket.emit('kickUser', user);
}
export function banUser(user) {
    socket.emit('banUser', user);
}

export function changeName(name) {
    socket.emit('changeName', name);
}

export function inviteUser(user) {
    socket.emit('inviteUser', user);
}

export function acceptInvite() {
    store.dispatch(closeFeedback('invite'));
    socket.emit('acceptInvite');
}

export function refuseInvite() {
    store.dispatch(closeFeedback('invite'));
    socket.emit('refuseInvite');
}

export function renameRoom(room) {
    socket.emit('renameRoom', room);
}

export function removeRoom(room) {
    socket.emit('removeRoom', room);
}

export function addRoom(roomName) {
    socket.emit('addRoom', roomName);
}

export function globalMessage(message) {
    socket.emit('globalMessage', message);
}
export function moveAllTo(room) {
    socket.emit('moveAllTo', room);
}

export function toggleHelp() {
    socket.emit('toggleHelp');
}

export function closeSocket() {
    if(socket) {
        socket.close();
    }
    if(jitsiApi) {
        jitsiApi.dispose();
    }
}

window.onbeforeunload = function(){
    closeSocket();
 }