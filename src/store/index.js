import { configureStore } from "@reduxjs/toolkit";
import { socketReducer } from "./socket-slice";

export const store = configureStore({
    reducer: {
        socket: socketReducer
    }
});