import { createSlice } from "@reduxjs/toolkit";

const socketSlice = createSlice({
    name: 'socket',
    initialState: {
        user: {
            id:null,
            name:localStorage.getItem('username') ? localStorage.getItem('username'):'',
            room: null,
            admin: false,
            userId: null,
            help: false
        },
        users: [],
        roomList: [],
        feedbacks: {
            invite: null,
            ban: null,
            rmRoom: null,
            renameRoom: null,
            addRoom: null,
            alertMsg: null,
            globalMsg: null
        }
    },
    reducers: {
        updateUser(state, {payload}) {
            state.user = payload;
        },
        changeUserName(state, {payload}) {
            state.user.name = payload;
        },
        updateUsersList(state, {payload}) {
            state.users = payload;
        },
        updateRoomList(state, {payload}) {
            state.roomList = payload;
        },
        openFeedback(state, {payload}) {
            state.feedbacks[payload.feedback] = payload.value;
        },
        closeFeedback(state, {payload}) {
            state.feedbacks[payload] = null;
        }
    }
});

export const {updateUser, updateUsersList, changeUserName, updateRoomList, closeFeedback, openFeedback} = socketSlice.actions;

export const socketReducer = socketSlice.reducer;